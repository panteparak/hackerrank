import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

public class GameOfTwoStacks {
    static class FastScanner {
        private BufferedReader bufferedReader;
        private StringTokenizer tokenizer;

        public FastScanner(InputStream in) {
            this.bufferedReader = new BufferedReader(new InputStreamReader(in));
        }

        public String next(){
            if (tokenizer == null || ! tokenizer.hasMoreTokens()){
                try {

                    tokenizer = new StringTokenizer(bufferedReader.readLine());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return tokenizer.nextToken();
        }

        public Integer nextInt(){
            return Integer.valueOf(next());
        }
    }

    public static void main(String[] args) {
        FastScanner scanner = new FastScanner(System.in);
        int ROUND = scanner.nextInt();

        for (int i = 0; i < ROUND; i++){
            int stackASize = scanner.nextInt();
            int stackBSize = scanner.nextInt();
            int K = scanner.nextInt();

            Integer[] lstA = new Integer[stackASize];
            Integer[] lstB = new Integer[stackBSize];


            for (int j = 0; j < stackASize; j++){
                int n = scanner.nextInt();
                lstA[j] = n;
            }
            for (int j = 0; j < stackBSize; j++){
                int n = scanner.nextInt();
                lstB[j] = n;
            }

            System.out.println(solve(lstA, lstB, K));
        }
    }

    private static int solve(Integer[] lstA, Integer[] lstB, Integer K){

        int sum = 0, i_lstA = 0, i_lstB = 0;

        while (i_lstA < lstA.length && sum + lstA[i_lstA] <= K){
            sum += lstA[i_lstA++];
        }

        int n = i_lstA;
        while (i_lstB < lstB.length && i_lstA >= 0) {
            sum += lstB[i_lstB++];
            while (sum > K && i_lstA > 0) {
                sum -= lstA[--i_lstA];
            }
            if (sum <= K && n < (i_lstA + i_lstB)) {
                n = i_lstA + i_lstB;
            }
        }
        return n;
    }
}
