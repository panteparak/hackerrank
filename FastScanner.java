import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

/**
 * Created by panteparak on 7/1/17.
 */
public class FastScanner {

    private BufferedReader reader;
    private StringTokenizer token;

    public FastScanner(InputStream in) {
        this.reader = new BufferedReader(new InputStreamReader(in));
    }

    private String next(){
        if (this.token == null || ! this.token.hasMoreTokens()){
            try {
                this.token = new StringTokenizer(reader.readLine());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return this.token.nextToken();
    }

    public Integer nextInt(){
        return Integer.valueOf(next());
    }

    public Long nextLong(){
        return Long.valueOf(next());
    }

    public String nextString(){
        return next();
    }
}
