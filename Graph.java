import java.util.*;

/**
 * Created by panteparak on 6/30/17.
 */
public class Graph<T extends Comparable<T>> {
    private Map<T, Set<T>> graph;
    private Set<T> uniqueNodes;
    private Integer edges;

    public Graph() {
        this.graph = new HashMap<>();
        this.edges = 0;
        this.uniqueNodes = new HashSet<>();
    }

    public void addNode(T u){
        uniqueNodes.add(u);
        if (! graph.containsKey(u)) graph.put(u, new HashSet<>());

    }

    public void addNode(T u, T v){
        addNode(u);
        graph.get(u).add(v);
        this.edges++;
    }

    public Set<T> getAllVertex(){
        return uniqueNodes;
    }

    public boolean containVertex(T u){
        return graph.containsKey(u);
    }

    public Set<T> vertexOf(T u){
        return graph.get(u);
    }

    public Integer size(){
        return uniqueNodes.size();
    }

    public Integer edge(){
        return edges;
    }


    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("Graph {").append("\n");
        Iterator<T> itr = graph.keySet().iterator();
        while (itr.hasNext()) {
            T next = itr.next();
            builder.append("\t").append(next).append(" -> ").append(this.graph.get(next)).append("\n");
        }

        builder.append("}");

        return builder.toString();
    }
}