import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.StringTokenizer;

/**
 * Created by panteparak on 5/30/17.
 */
public class Pairs {
    static class FastScanner{
        private BufferedReader reader;
        private StringTokenizer tokenizer;

        public FastScanner(InputStream in) {
            this.reader = new BufferedReader(new InputStreamReader(in));
        }

        public String next(){
            if (tokenizer == null || !tokenizer.hasMoreTokens()) try {
                tokenizer = new StringTokenizer(this.reader.readLine());
            } catch (IOException e) {
                e.printStackTrace();
            }
            return this.tokenizer.nextToken();
        }

        public int nextInt(){
            return Integer.parseInt(next());
        }

        public long nextLong(){
            return Long.parseLong(next());
        }
    }
    public static void main(String[] args) {

        FastScanner scanner = new FastScanner(System.in);
        int n = scanner.nextInt();
        int k = scanner.nextInt();

        int[] numbersSorted = new int[n];
//        int[] numbersUnSorted = new int[n];

        for (int i = 0; i < n; i++){
            int m = scanner.nextInt();
            numbersSorted[i] = m;
//            numbersUnSorted[i] = m;
        }

        Arrays.sort(numbersSorted);

        int count = 0;
        for (int i = 0; i < numbersSorted.length; i++){
            int num = numbersSorted[i];

            if (num - k > 0 && hasNumber(numbersSorted, num - k)) count++;
//            if (num + k < Integer.MAX_VALUE && hasNumber(numbersSorted, num + k)) count++;
        }

        System.out.println(count);


    }

    private static boolean hasNumber(int[] array, int k){
        return binaryHelper(array, 0, array.length, k);
    }

    private static boolean binaryHelper(int[] array, int min, int max, int k){
        if (min >= max) return false;
        else {
            int mid = (min + max) / 2;

            if (array[mid] == k) return true;
            else if (array[mid] > k) return binaryHelper(array, min, mid, k);
            else return binaryHelper(array, mid + 1, max, k);
        }
    }
}
