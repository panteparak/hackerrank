import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

/**
 * Created by panteparak on 5/15/17.
 */
public class KFactorization {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);


        int N = scanner.nextInt();
        int K = scanner.nextInt();


        List<Integer> numberList = new ArrayList<>(K);

        for (int i = 0; i < K; i++){
            int readNum = scanner.nextInt();
            if (N % readNum == 0) numberList.add(readNum);
        }

        if (numberList.size() == 0){
            System.out.println(-1);
        }else{
            Collections.sort(numberList);
//            if (!numberList.contains(1)) numberList.add(0, 1);
//            if (!numberList.contains(N)) numberList.add(N);
//            System.out.println(numberList);
            List<Integer> out = runner(numberList, numberList.size() - 1,N, new ArrayList<>());
            if (out != null && out.size() > 0 && out.get(out.size() -1) == 1){
                Collections.reverse(out);

                for (int n : out){
                    System.out.print(n + " ");
                }
                System.out.println(N);
            }else {
                System.out.println("-1");
            }
        }
    }

//    static List<Integer> runner(List<Integer> numberList, int pointer,int n, List<Integer> result){
//        System.out.println(n + " " + pointer);
//        System.out.println(result);
//        System.out.println();
//        if (n == 1) return result;
//        else if (pointer < 0 || n < 1) return null;
//        else if (n % numberList.get(pointer) == 0){
//            result.add(pointer);
//            boolean con = runner(numberList, pointer, n/numberList.get(pointer), result) != null;
//            if (!con) System.out.println("RM: " + result.remove(pointer));
//            return result;
//        }else {
//            return runner(numberList, pointer -1, n, result);
//        }
//    }


//    TODO: Last Working
//    static List<Integer> runner(List<Integer> numberList, int index,int n, List<Integer> result){
//        System.out.println(result);
//        if (n == 1) return result;
//        else if (index < 0 || n < 1) return null;
//        else {
//            int currentNum = numberList.get(index);
//            Integer nn = n/currentNum;
//            result.add(nn);
//            boolean a = runner(numberList, index, nn, result) == null;
//            if (a) {
//                System.out.println("RM: " + nn);
//                result.remove(nn);
//                return runner(numberList, index - 1, n * nn, result);
//            }
//
//        }
//        return result;
//    }

    static List<Integer> runner(List<Integer> numberList, int index,int n, List<Integer> result){
        if (n == 1) return result;
        if (index < 0 || n < 1) return null;

        else {
            Integer currentNum = numberList.get(index);
            Integer nextDivide = n / currentNum;

            if (n % currentNum == 0){
                if (nextDivide > 0){
                    result.add(nextDivide);
                    if (runner(numberList, index, nextDivide, result) == null){
                        result.remove(nextDivide);
                        runner(numberList, index - 1, n, result);
                    }
                    return result;

                }
            }
            return runner(numberList, index-1, n, result);

//            if (nextDivide > 0){
//                result.add(nextDivide);
//                if (runner(numberList, index, nextDivide, result) == null){
//                    result.remove(nextDivide);
//
//                }
//
//                System.out.println(result);
//            }
//            return runner(numberList, index-1, n, result);
        }
    }
}
