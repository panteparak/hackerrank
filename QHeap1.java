import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.PriorityQueue;
import java.util.Stack;
import java.util.StringTokenizer;

/**
 * Created by panteparak on 6/12/17.
 */
public class QHeap1 {
    static class FastScanner {
        private StringTokenizer tokenizer;
        private BufferedReader bufferedReader;

        public FastScanner(InputStream in) {
            bufferedReader = new BufferedReader(new InputStreamReader(in));
        }

        private String next(){
            if (tokenizer == null || ! tokenizer.hasMoreTokens()){
                try {
                    tokenizer = new StringTokenizer(bufferedReader.readLine());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return tokenizer.nextToken();
        }

        public String nextString(){
            return next();
        }


        public Integer nextInt(){
            return Integer.parseInt(next());
        }

    }

    public static void main(String[] args) {
        FastScanner scanner = new FastScanner(System.in);
        int N = scanner.nextInt();
        Stack<Integer> stack = new Stack<>();
        PriorityQueue pq = new PriorityQueue();
        for (int i = 0; i < N; i++){
            int cmd = scanner.nextInt();

            if (cmd == 1){
                Integer num = scanner.nextInt();
                stack.add(num);
                pq.add(num);
            } else if (cmd == 2){
                int num = scanner.nextInt();
                stack.remove((Integer) num);
                pq.remove(num);
            }else {
                System.out.println(pq.peek());
            }
        }
    }
}
