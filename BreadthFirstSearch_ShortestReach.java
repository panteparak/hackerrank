import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.*;

/**
 * Created by panteparak on 6/28/17.
 */
public class BreadthFirstSearch_ShortestReach {
    static class FastScanner {
        private BufferedReader reader;
        private StringTokenizer token;

        public FastScanner(InputStream in) {
            reader = new BufferedReader(new InputStreamReader(in));
        }

        private String next() {
            if (token == null || ! token.hasMoreTokens()){
                try {
                    token = new StringTokenizer(reader.readLine());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return token.nextToken();
        }

        public Integer nextInt(){
            return Integer.parseInt(next());
        }
    }

    public static void main(String[] args){
        FastScanner scanner = new FastScanner(System.in);

        int testCaseCount = scanner.nextInt();
        for (int c = 0; c < testCaseCount; c++){
            int vertexCount = scanner.nextInt();
            int edgeCount = scanner.nextInt();
            Integer[] result = new Integer[vertexCount];
            Arrays.fill(result, -1);

            Map<Integer, Set<Integer>> graph = new HashMap<>();
            for (int i = 0; i < edgeCount; i++){
                int u = scanner.nextInt();
                int v = scanner.nextInt();

                if (! graph.containsKey(u)) graph.put(u, new HashSet<>());
                graph.get(u).add(v);

                if (! graph.containsKey(v)) graph.put(v, new HashSet<>());
                graph.get(v).add(u);
            }

            int start = scanner.nextInt();


            bfs(graph, new HashSet<>(), start, result);

            for (int n = 0; n < result.length; n++) if (result[n] != null) System.out.print(result[n] + " ");
            System.out.println();

        }
    }

    static class Pair {
        private Integer number;
        private Integer depth;

        public Pair(Integer number, Integer depth) {
            this.number = number;
            this.depth = depth;
        }

        @Override
        public String toString() {
            return "Pair{" +
                    "number=" + number +
                    ", depth=" + depth +
                    '}';
        }
    }

    private static Integer[] bfs(Map<Integer, Set<Integer>> graph, Set<Integer> visited,  Integer start, Integer[] result){
        LinkedList<Pair> traverse = new LinkedList<>();
        traverse.add(new Pair(start, 0));

        result[start-1] = null;
        while (! traverse.isEmpty()){
            Pair node = traverse.pop();

            if (result[node.number-1] != null && (result[node.number-1] > (node.depth * 6) || result[node.number-1] == -1)){
                result[node.number-1] = node.depth * 6;
            }

            if (! visited.contains(node.number)) {
                visited.add(node.number);
                if (graph.containsKey(node.number)) {
                    Set<Integer> next = graph.get(node.number);

                    Iterator<Integer> itr = next.iterator();
                    while (itr.hasNext()){
                        Integer num = itr.next();
                        traverse.add(new Pair(num, node.depth + 1));
                    }
                }
            }
        }
        return result;
    }
}
