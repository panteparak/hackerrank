import java.util.Scanner;

// https://www.hackerrank.com/challenges/simple-array-sum
public class SimpleArraySum {
  public static void main(String[] args) {
    Scanner s = new Scanner(System.in);
    int arrLen = s.nextInt();
    int n = 0;
    for (int i = 0; i < arrLen; i++){
      n += s.nextInt();
    }
    System.out.println(n);
  }
}
