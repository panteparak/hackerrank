/**
 * Created by panteparak on 5/7/17.
 */
public class PrintTheElementsOfALinkedList {

  void Print(Node head) {

    if (head == null){
      return;
    }else{
      System.out.println(head.data);
      Print(head.next);
    }
  }

  class Node {
    int data;
    Node next;
  }
}
