import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class RoadsAndLibraries {
    static class Graph<T extends Comparable<T>> {
        private Map<T, Set<T>> graph;
        private Set<T> uniqueNodes;
        private Integer edges;

        public Graph() {
            this.graph = new HashMap<>();
            this.edges = 0;
            this.uniqueNodes = new HashSet<>();
        }

        public void addNode(T u){
            uniqueNodes.add(u);
            if (! graph.containsKey(u)) graph.put(u, new HashSet<>());

        }

        public void addNode(T u, T v){
            addNode(u);
            graph.get(u).add(v);
            this.edges++;
        }

        public Set<T> getAllVertex(){
            return uniqueNodes;
        }

        public boolean containVertex(T u){
            return graph.containsKey(u);
        }

        public Set<T> vertexOf(T u){
            return graph.get(u);
        }

        public Integer size(){
            return uniqueNodes.size();
        }

        public Integer edge(){
            return edges;
        }


        @Override
        public String toString() {
            StringBuilder builder = new StringBuilder();
            builder.append("Graph {").append("\n");
            Iterator<T> itr = graph.keySet().iterator();
            while (itr.hasNext()) {
                T next = itr.next();
                builder.append("\t").append(next).append(" -> ").append(this.graph.get(next)).append("\n");
            }

            builder.append("}");

            return builder.toString();
        }
    }
    
    static class FastScanner {
        private BufferedReader reader;
        private StringTokenizer token;

        public FastScanner(InputStream in) {
            this.reader = new BufferedReader(new InputStreamReader(in));
        }

        private String next(){
            if (this.token == null || ! this.token.hasMoreTokens()){
                try {
                    this.token = new StringTokenizer(reader.readLine());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return this.token.nextToken();
        }

        public Integer nextInt(){
            return Integer.valueOf(next());
        }

        public Long nextLong(){
            return Long.valueOf(next());
        }

        public String nextString(){
            return next();
        }
    }
    public static void main(String[] args) {
        FastScanner scanner = new FastScanner(System.in);
        int Q = scanner.nextInt();

        for (int i = 0; i < Q; i++){
            int nCities = scanner.nextInt();
            int mRoads = scanner.nextInt();
            long costLibrary = scanner.nextLong();
            long costRoad = scanner.nextLong();


            Graph<Integer> graph = new Graph<>();
            for (int r = 0; r < mRoads; r++){
                int u = scanner.nextInt()-1;
                int v = scanner.nextInt()-1;

                graph.addNode(u,v);
                graph.addNode(v, u);
            }

            System.out.println(calculator(graph, costLibrary, costRoad, nCities));
        }

    }

    private static Long calculator(Graph<Integer> graph, Long libraryCost, Long roadCost, Integer nodeCount){
        Long count = 0L;
        Set<Integer> visited = new HashSet<>();
        for (int i = 0; i < nodeCount; i++){
            if (! visited.contains(i)){
                Long c = countGraph(graph, visited, i);

                Long a = libraryCost + (c - 1) * roadCost;
                Long b = libraryCost * c;

                count += Math.min(a, b);
            }
        }
        return count;

    }

    private static Long countGraph(Graph<Integer> graph, Set<Integer> visited, Integer start){
        Stack<Integer> stack = new Stack<>();
        stack.add(start);
        Long count = 0L;

        while (! stack.isEmpty()){
            Integer node = stack.pop();
            if (! visited.contains(node)){
                visited.add(node);
                count++;
                if (graph.containVertex(node)){
                    stack.addAll(graph.vertexOf(node));

                }
            }
        }
        return count;
    }
}

