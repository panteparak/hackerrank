import re

def init_ipv4():
  return re.compile(r"^((25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$")

def init_ipv6():
  return re.compile(r"^([0-f]{,4}:){7}[0-f]{,4}$")
def main():
  size = int(input())
  v4, v6 = init_ipv4(), init_ipv6()
  for _ in range(size):
    payload = input()
    if (v4.match(payload)):
      print("IPv4")
    elif (v6.match(payload)):
      print("IPv6")
    else:
      print("Neither")


if __name__ == '__main__':
  main()
