import java.util.Scanner;

// https://www.hackerrank.com/challenges/compare-the-triplets
public class CompareTheTriplets {
  static int[] solve(int a0, int a1, int a2, int b0, int b1, int b2){
    // Complete this function
    int a = 0;
    int b = 0;
    int[] score = {0, 0};
    if (a0 != b0){
      if (a0 < b0) score[1]++;
      else score[0]++;
    }
    if (a1 != b1){
      if (a1 < b1) score[1]++;
      else score[0]++;
    }
    if (a2 != b2){
      if (a2 < b2) score[1]++;
      else score[0]++;
    }

    return score;
  }

  public static void main(String[] args) {
    Scanner in = new Scanner(System.in);
    int a0 = in.nextInt();
    int a1 = in.nextInt();
    int a2 = in.nextInt();
    int b0 = in.nextInt();
    int b1 = in.nextInt();
    int b2 = in.nextInt();
    int[] result = solve(a0, a1, a2, b0, b1, b2);
    String separator = "", delimiter = " ";
    for (Integer value : result) {
      System.out.print(separator + value);
      separator = delimiter;
    }
    System.out.println("");
  }
}
