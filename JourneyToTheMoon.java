import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.*;

/**
 * Created by panteparak on 6/30/17.
 */
public class JourneyToTheMoon { 
   static class FastScanner {
        private BufferedReader reader;
        private StringTokenizer token;

        public FastScanner(InputStream in) {
            reader = new BufferedReader(new InputStreamReader(in));
        }

        private String next(){
            if (token == null || ! token.hasMoreTokens()){
                try {
                    token = new StringTokenizer(reader.readLine());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            return token.nextToken();
        }

        public Integer nextInt(){
            return Integer.valueOf(next());
        }
    }

    public static void main(String[] args) {
        FastScanner scanner = new FastScanner(System.in);

        int N = scanner.nextInt();
        int P = scanner.nextInt();

        Graph<Integer> graph = new Graph<>();

        for (int i = 0; i < N; i++){
            graph.addNode(i);
        }

        for (int c = 0; c < P; c++){
            int u = scanner.nextInt();
            int v = scanner.nextInt();

            graph.addNode(u, v);
            graph.addNode(v, u);
        }

        System.out.println(countUnconnectedGraphs(graph));
    }

    private static Long countUnconnectedGraphs(Graph<Integer> graph){
        Iterator<Integer> itr = graph.getAllVertex().iterator();
        Set<Integer> visited = new HashSet<>();

        long count = 0;
        int size = graph.size();
        while (itr.hasNext()){
            Integer node = DFS(graph, visited, itr.next());
            if (node > 0) {
                count += node * (size -= node);
            }
        }
        return count;
    }

    private static Integer DFS(Graph<Integer> graph, Set<Integer> visited, Integer start) {
        Stack<Integer> stack = new Stack<>();
        stack.add(start);

        Integer count = 0;

        while (! stack.isEmpty()){
            Integer node = stack.pop();

            if (! visited.contains(node)) {
                visited.add(node);
                count++;
                if (graph.containVertex(node)) {
                    stack.addAll(graph.vertexOf(node));
                }
            }
        }

        return count;
    }
    
    static class Graph<T extends Comparable<T>> implements Comparator<T> {
        private Map<T, Set<T>> graph;
        private Set<T> uniqueNodes;
        private Integer edges;

        public Graph() {
            this.graph = new HashMap<>();
            this.edges = 0;
            this.uniqueNodes = new HashSet<>();
        }

        public void addNode(T u){
            uniqueNodes.add(u);
            if (! graph.containsKey(u)) graph.put(u, new HashSet<>());

        }

        public void addNode(T u, T v){
            addNode(u);
            graph.get(u).add(v);
            this.edges++;
        }

        public Set<T> getAllVertex(){
            return uniqueNodes;
        }

        public boolean containVertex(T u){
            return graph.containsKey(u);
        }

        public Set<T> vertexOf(T u){
            return graph.get(u);
        }

        public Integer size(){
            return uniqueNodes.size();
        }

        public Integer edge(){
            return edges;
        }


        @Override
        public String toString() {
            StringBuilder builder = new StringBuilder();
            builder.append("Graph {").append("\n");
            Iterator<T> itr = graph.keySet().iterator();
            while (itr.hasNext()) {
                T next = itr.next();
                builder.append("\t").append(next).append(" -> ").append(this.graph.get(next)).append("\n");
            }

            builder.append("}");

            return builder.toString();
        }
        @Override
        public int compare(T o1, T o2) {
            return o1.compareTo(o2);
        }
    }
}
