import java.util.Scanner;

// https://www.hackerrank.com/challenges/larrys-array
public class LarrysArray {
  public static void main(String[] args) {
    Scanner s = new Scanner(System.in);
    int testCaseLen = s.nextInt();

    for (int i = 0; i < testCaseLen; i++){
      int arrLen = s.nextInt();
      int[] arr = new int[arrLen];
      for (int n = 0; n < arrLen; n++){
        arr[n] = s.nextInt();
      }
      System.out.println(solve(arr) ? "YES" : "NO");
    }
  }

  static boolean solve(int[] arr){
    boolean solving = true;

    while (solving){
      boolean swap = false;
      for (int i = 0; i < arr.length - 2; i++){
        int smallestTripleIndex;

        if (arr[i] < arr[i+1] && arr[i] < arr[i+2]) smallestTripleIndex = i;
        else if (arr[i+1] < arr[i] && arr[i+1] < arr[i+2]) smallestTripleIndex = i + 1;
        else smallestTripleIndex = i+2;

        if (smallestTripleIndex - i > 0){
          swap = true;
          int A = arr[i];
          int B = arr[i+1];
          int C = arr[i+2];

          if (smallestTripleIndex - i == 1){ // One Swap
            arr[i] = B;
            arr[i+1] = C;
            arr[i+2] = A;
          } else { // two swaps
            arr[i] = C;
            arr[i+1] = A;
            arr[i+2] = B;
          }
        }
      }
      solving = swap;
    }
    return isSorted(arr);
  }

  static boolean isSorted(int[] arr){
    for (int i = 0; i < arr.length - 1; i++)
      if (!(arr[i] < arr[i + 1]))
        return false;
    return true;
  }
}
