import re

def init_tag():
  return re.compile(r'<a.*?href=\"(.*?)\".*?>(.*?)<\/a>', re.M)

def init_extractor():
  return re.compile(r'<.*?>(.*?)<\/.*?>')

def main():
  N = int(input())
  tag, extractor = init_tag(), init_extractor()
  
  for _ in range(N):
    payload = input()
    matches = tag.findall(payload.strip())
    for link, txt in matches:
      print("{0},{1}".format(link, re.sub(r'<.*?>', "", txt.strip())))
 
if __name__ == '__main__':
  main()
  
