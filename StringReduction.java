import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.StringTokenizer;

public class StringReduction {
    static class FastScanner {
        private BufferedReader reader;
        private StringTokenizer token;

        public FastScanner(InputStream in) {
            reader = new BufferedReader(new InputStreamReader(in));
        }

        private String next() {
            while (token == null || !token.hasMoreTokens() ) {
                try {
                    token = new StringTokenizer(reader.readLine());
                } catch (IOException e) {
                }
            }
            return token.nextToken();
        }

        public String nextString(){
            return next();
        }

        public Integer nextInt(){
            return Integer.parseInt(next());
        }

        public Double nextDouble(){
            return Double.parseDouble(next());
        }

        public Long nextLong(){
            return Long.parseLong(next());
        }

        public BigInteger nextBigInteger(){
            return new BigInteger(next());
        }
    }
    public static void main(String[] args) {
        FastScanner fs = new FastScanner(System.in);
        int n = fs.nextInt() + 1;
        int m = fs.nextInt() + 1;

        int[] N = new int[n];
        for (int i = 1; i < n; i++){
            N[i] = fs.nextInt();
        }

        int[] M = new int[m];
        for (int i = 1; i < m; i++){
            M[i] = fs.nextInt();
        }

        int[][] ANS = new int[n][m];
        for (int i = 0; i < ANS.length; i++)
            Arrays.fill(ANS[i], 0);

        LCS(N, M, ANS);
        List<Integer> lst = new ArrayList<>(n * m);
        backtrack(N, M, ANS, n - 1, m - 1, lst);
        System.out.println(lst);

    }

    private static void LCS(int[] A, int[] B, int[][] ANS){
        for (int i = 0; i < ANS.length; i++){
            for (int j = 0; j < ANS[i].length; j++) {
                if (i == 0 || j == 0)
                    continue;
                else if (A[i] == B[j])
                    ANS[i][j] = ANS[i - 1][j - 1] + 1;
                else
                    ANS[i][j] = Math.max(ANS[i - 1][j], ANS[i][j - 1]);
            }
        }
    }

    private static void backtrack(int[] A, int[] B, int[][] ANS, int i, int j, List<Integer> lst){
        if (i == 0 || j == 0) lst.clear();
        else if (A[i] == B[j]){
            backtrack(A, B, ANS, i - 1, j - 1, lst);
            lst.add(A[i]);
        }else if (ANS[i][j] == ANS[i - 1][j])
            backtrack(A, B, ANS, i - 1, j, lst);
        else backtrack(A, B, ANS, i, j - 1, lst);
    }
}
