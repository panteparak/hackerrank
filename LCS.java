import java.math.*;
import java.util.*;
import java.io.*;

public class LCS {
    static class FastScanner {
        private BufferedReader reader;
        private StringTokenizer token;

        public FastScanner(InputStream in) {
            reader = new BufferedReader(new InputStreamReader(in));
        }

        private String next() {
            while (token == null || !token.hasMoreTokens() ) {
                try {
                    token = new StringTokenizer(reader.readLine());
                } catch (IOException e) {
                }
            }
            return token.nextToken();
        }

        public String nextString(){
            return next();
        }

        public Integer nextInt(){
            return Integer.parseInt(next());
        }

        public Double nextDouble(){
            return Double.parseDouble(next());
        }

        public Long nextLong(){
            return Long.parseLong(next());
        }

        public BigInteger nextBigInteger(){
            return new BigInteger(next());
        }
    }

    public static void main(String[] args) {
        FastScanner fs = new FastScanner(System.in);

        int n = fs.nextInt(), m = fs.nextInt();

        int[] A = new int[n + 1];
        int[] B = new int[m + 1];
        for (int i = 1; i < A.length; i++){
            A[i] = fs.nextInt();
        }

        for (int i = 1; i < B.length; i++){
            B[i] = fs.nextInt();
        }

        System.out.println(Arrays.toString(A));
        System.out.println(Arrays.toString(B));


        int[][] F = new int[n + 1][m + 1];


        for (int i = 0; i < F.length; i++) {
            for (int j = 0; j < F[i].length; j++) {

                if (i == 0 || j == 0) {
                    F[i][j] = 0;
                } else if (A[i] == B[j]) {
                    F[i][j] = 1 + F[i-1][j-1];
                } else {
                    F[i][j] = Math.max(F[i-1][j], F[i][j-1]);
                }
            }
        }

        List<Integer> lst = new ArrayList<>(n * m);
        solve(A, B, F, n, m, lst);

        StringBuilder sb = new StringBuilder();
        for (Integer integer : lst){
            sb.append(integer).append(" ");
        }
        System.out.println(sb.toString().trim());
    }

    private static void solve(int[] A, int[] B, int[][] F, int i, int j, List<Integer> lst){
        if (i == 0 || j == 0) {
            lst.clear();
        } else if (A[i] == B[j]) {
            solve(A, B, F, i - 1, j - 1, lst);
            lst.add(A[i]);
        } else if (F[i][j] == F[i-1][j]) {
            solve(A, B, F,i - 1, j, lst);
        } else {
            solve(A, B, F, i, j - 1, lst);
        }
    }
}