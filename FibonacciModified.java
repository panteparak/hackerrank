import java.io.*;
import java.math.BigInteger;
import java.util.*;

public class FibonacciModified {
    static class FastScanner {
        private BufferedReader reader;
        private StringTokenizer token;

        public FastScanner(InputStream in) {
            reader = new BufferedReader(new InputStreamReader(in));
        }

        private String next() {
            while (token == null || !token.hasMoreTokens() ) {
                try {
                    token = new StringTokenizer(reader.readLine());
                } catch (IOException e) {
                }
            }
            return token.nextToken();
        }

        public Integer nextInt(){
            return Integer.parseInt(next());
        }

        public BigInteger nextBigInteger(){
            return new BigInteger(next());
        }
    }

    private static BigInteger solve(BigInteger n1, BigInteger n2, int n){
        if (n == 1) return n1;
        if (n == 2) return n2;
        return solve(n2, (n2.multiply(n2).add(n1)), n-1);
    }

    public static void main(String[] args) {
        FastScanner fs = new FastScanner(System.in);
        BigInteger n1 = fs.nextBigInteger();
        BigInteger n2 = fs.nextBigInteger();
        int n = fs.nextInt();
        System.out.printf("%s\n", solve(n1, n2, n).toString());
    }
}
