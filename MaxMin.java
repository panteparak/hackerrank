import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;


public class MaxMin {
  public static void main(String[] args) throws NumberFormatException, IOException {
    BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
    int N = Integer.parseInt(in.readLine());
    int K = Integer.parseInt(in.readLine());

    int[] arr = new int[N];

    for (int i = 0; i < arr.length; i++) {
      arr[i] = Integer.parseInt(in.readLine());
    }
    Arrays.sort(arr);
    System.out.println(solver(arr, K));
  }

  static int solver(int[] arr, int K){
    int n = Integer.MAX_VALUE;
    for (int i = 0; i < arr.length-1; i++){
      for (int j = i + 1; j < arr.length; j++){
        int nn;
        if (j - i == K-1 && (( nn = arr[j] - arr[i]) < n)){
          n = nn;
        }
      }
    }
    return n;
  }
}
