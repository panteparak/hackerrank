import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class TheFullCountingSort {
    static class FastScanner {
        private BufferedReader bufferedReader;
        private String[] cache;
        private int i;

        public FastScanner(InputStream in) {
            this.bufferedReader = new BufferedReader(new InputStreamReader(in));
        }

        private String next(){
            if (cache == null || i >= cache.length){
                try {
                    cache = bufferedReader.readLine().split("\\s");
                } catch (IOException e) {
                    e.printStackTrace();
                }
                i = 0;
            }
            return cache[i++];
        }

        public String nextString(){
            return next();
        }

        public Integer nextInt(){
            return Integer.valueOf(next());
        }
    }

    public static void main(String[] args) {
        FastScanner scanner = new FastScanner(System.in);
        int N = scanner.nextInt();
        StringBuilder[] stringBuilders = new StringBuilder[100];
        for (int i = 0; i < stringBuilders.length; i++){
            stringBuilders[i] = new StringBuilder(100000);
        }

        for (int i = 0; i < N; i++){
            int n = scanner.nextInt();
            String st = scanner.nextString();
            stringBuilders[n].append(i < N/2 ? "-" : st).append(" ");
        }

        for (int i = 0; i < stringBuilders.length; i++){
            System.out.println(stringBuilders[i]);
        }
    }
}
