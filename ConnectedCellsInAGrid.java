import java.io.*;
import java.util.*;

public class ConnectedCellsInAGrid {
    static class FastScanner {

        private BufferedReader reader;
        private StringTokenizer token;

        public FastScanner(InputStream in) {
            this.reader = new BufferedReader(new InputStreamReader(in));
        }

        private String next(){
            if (this.token == null || ! this.token.hasMoreTokens()){
                try {
                    this.token = new StringTokenizer(reader.readLine());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return this.token.nextToken();
        }

        public Integer nextInt(){
            return Integer.valueOf(next());
        }

        public Long nextLong(){
            return Long.valueOf(next());
        }

        public String nextString(){
            return next();
        }
    }
    static int row;
    static int col;
    public static void main(String[] args) {
        FastScanner scanner = new FastScanner(System.in);
        row = scanner.nextInt();
        col = scanner.nextInt();

        boolean[][] board = new boolean[row][col];
//        Point start = new Point(0,0);
        for (int i = 0; i < row; i++) for (int j = 0; j < col; j++){
            board[i][j] = scanner.nextInt() == 1;

        }

        boolean[][] visited = new boolean[row][col];
        int count = 0;

        for (int i = 0; i < row; i++) for (int j = 0; j < col; j++){
            if (!visited[i][j] && board[i][j]){
                int t = solve(board, visited, new Point(i, j));
                if (t > count) count = t;
            }
        }
        System.out.println(count);

    }

    static class Point {
        private int row;
        private int col;

        public Point(int row, int col) {
            this.row = row;
            this.col = col;
        }

        @Override
        public String toString() {
            return "Point{" +
                    "row=" + row +
                    ", col=" + col +
                    '}';
        }
    }

    private static int solve(boolean[][] board, boolean[][] visited, Point start){
        Queue<Point> queue = new ArrayDeque<>();
        queue.add(start);
        int count = 0;


        while (! queue.isEmpty()){
            Point node = queue.poll();
            //System.out.println(node);
            if (!visited[node.row][node.col]){
                visited[node.row][node.col] = true;
                count++;

                //            down
                if ((node.row + 1 < row) && board[node.row + 1][node.col] && !visited[node.row + 1][node.col]){
                    Point point = new Point(node.row+1,node.col);
                    queue.add(point);
                    //System.out.println("add" + point);
                }

//            up
                if ((node.row - 1 >= 0) && board[node.row - 1][node.col] && !visited[node.row - 1][node.col]){
                    Point point = new Point(node.row-1,node.col);
                    queue.add(point);
                    //System.out.println("add" + point);

                }

//            right
                if ((node.col + 1 < col) && board[node.row][node.col+1] && !visited[node.row][node.col+1]){
                    Point point = new Point(node.row,node.col +1);
                    queue.add(point);
                    //System.out.println("add" + point);
                }

//            left
                if ((node.col -1) >= 0 && board[node.row][node.col - 1] && !visited[node.row][node.col-1]){
                    Point point = new Point(node.row,node.col - 1);
                    queue.add(point);
                    //System.out.println("add" + point);
                }

//            right down
                if ((node.col +1) < col && (node.row + 1) < row && board[node.row+1][node.col + 1] && !visited[node.row + 1][node.col+1]){
                    Point point = new Point(node.row + 1,node.col + 1);
                    queue.add(point);
                }

//            left up
                if ((node.col -1) >= 0 && (node.row - 1) >= 0 && board[node.row- 1][node.col - 1] && !visited[node.row- 1][node.col-1]){
                    Point point = new Point(node.row - 1,node.col - 1);
                    queue.add(point);
                }

//            right up
                if ((node.col + 1) < col && (node.row - 1) >= 0 && board[node.row-1][node.col + 1] && !visited[node.row - 1][node.col+1]){
                    Point point = new Point(node.row - 1,node.col + 1);
                    queue.add(point);
                }

//            left down
                if ((node.col -1) >= 0 && (node.row + 1) < row && board[node.row+1][node.col - 1] && !visited[node.row + 1][node.col-1]){
                    Point point = new Point(node.row + 1,node.col - 1);
                    queue.add(point);
                }
            }
        }

        return count;
    }
}
