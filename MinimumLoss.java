import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;

/**
 * Created by panteparak on 5/29/17.
 */
public class MinimumLoss {
    static class FastScanner{
        private BufferedReader reader;
        private StringTokenizer tokenizer;

        public FastScanner(InputStream in) {
            this.reader = new BufferedReader(new InputStreamReader(in));
        }

        public String next(){
            if (tokenizer == null || !tokenizer.hasMoreTokens()) try {
                tokenizer = new StringTokenizer(this.reader.readLine());
            } catch (IOException e) {
                e.printStackTrace();
            }
            return this.tokenizer.nextToken();
        }

        public int nextInt(){
            return Integer.parseInt(next());
        }

        public long nextLong(){
            return Long.parseLong(next());
        }
    }


    public static void main(String[] args) {
        FastScanner scanner = new FastScanner(System.in);

        int nSize = scanner.nextInt();

        Map<Long, Integer> daysMap = new HashMap<>(nSize);
        long[] arrNumbers = new long[nSize];

        for (int i = 0; i < nSize; i++){
            arrNumbers[i] = scanner.nextLong();
            daysMap.put(arrNumbers[i], i);
        }
        Arrays.sort(arrNumbers);
        Long minLoss = Long.MAX_VALUE;

        for (int i = 0; i < arrNumbers.length - 1; i++){
            long currentDay = daysMap.get(arrNumbers[i]);
            long nextDay = daysMap.get(arrNumbers[i+1]);

            if (((currentDay > nextDay) && (arrNumbers[i+1] - arrNumbers[i]) < minLoss) && arrNumbers[i + 1] - arrNumbers[i] > 0){
                minLoss = arrNumbers[i + 1] - arrNumbers[i];
            }
        }

        System.out.println(minLoss);
    }
}
