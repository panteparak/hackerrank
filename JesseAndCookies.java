import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.PriorityQueue;
import java.util.StringTokenizer;

public class JesseAndCookies {

    static class FastScanner {
        private BufferedReader bufferedReader;
        private StringTokenizer tokenizer;

        public FastScanner(InputStream in) {
            this.bufferedReader = new BufferedReader(new InputStreamReader(in));
        }

        public String next(){
            if (tokenizer == null || ! tokenizer.hasMoreTokens()){
                try {
                    tokenizer = new StringTokenizer(bufferedReader.readLine());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return tokenizer.nextToken();
        }

        public Integer nextInt(){
            return Integer.valueOf(next());
        }
    }

    public static void main(String[] args) {
        FastScanner scanner = new FastScanner(System.in);
        int N = scanner.nextInt();
        int K = scanner.nextInt();

        PriorityQueue<Integer> pq = new PriorityQueue<>(N);

        for (int i = 0; i < N; i++){
            int n = scanner.nextInt();
            pq.add(n);
        }

        System.out.println(solve(pq, K));


    }

    private static int solve(PriorityQueue<Integer> pq, int K){
        int count = 0;

        while (pq.peek() < K){
            if (pq.size() < 2) return -1;
            else {
                int n = pq.poll();
                int m = pq.poll();
                pq.add(n + (m * 2));
                count++;
            }
        }

        return count;
    }
}
