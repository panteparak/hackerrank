import java.math.BigInteger;
import java.util.Scanner;

//  https://www.hackerrank.com/challenges/recursive-digit-sum
public class RecursiveDigitSum {
  public static void main(String[] args) {
    Scanner s = new Scanner(System.in);
    String a = s.next();
    int multiply = s.nextInt();

    BigInteger num = solver(new BigInteger(a), multiply);
    System.out.println(num);
  }

  static BigInteger solver(BigInteger n, int multiplier){
    if (n.toString().length() == 1 && multiplier == 0){
      return n;
    } else {
      String ss = n.toString();
      BigInteger bigInteger = BigInteger.ZERO;
      for (int i = 0; i < ss.length(); i++){
        String s = ss.substring(i, i+1);
        bigInteger = bigInteger.add(BigInteger.valueOf(Long.valueOf(s)));
      }
      if (multiplier > 0) bigInteger = bigInteger.multiply(BigInteger.valueOf(multiplier));
      return solver(bigInteger, 0);
    }
  }
}
