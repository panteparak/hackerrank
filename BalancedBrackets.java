import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.*;

/**
 * Created by panteparak on 6/9/17.
 */
public class BalancedBrackets {
    static class FastScanner {
        private StringTokenizer tokenizer;
        private BufferedReader bufferedReader;

        public FastScanner(InputStream in) {
            bufferedReader = new BufferedReader(new InputStreamReader(in));
        }

        private String next(){
            if (tokenizer == null || ! tokenizer.hasMoreTokens()){
                try {
                    tokenizer = new StringTokenizer(bufferedReader.readLine());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return tokenizer.nextToken();
        }

        public String nextString(){
            return next();
        }

        public Integer nextInt(){
            return Integer.parseInt(next());
        }

    }

    public static void main(String[] args) {
        FastScanner scanner = new FastScanner(System.in);
        int N = scanner.nextInt();
        for (int i = 0; i < N; i++){
            LinkedList<String> lst = new LinkedList<>();
            lst.addAll(Arrays.asList(scanner.nextString().split("")));
            System.out.println(solve(lst) ? "YES" : "NO");
        }
    }

    private static boolean solve(LinkedList<String> lst){
        if (lst.size() % 2 != 0) return false;
        else {
            Stack<String> stack = new Stack<>();

            while (! lst.isEmpty()){
                String token = lst.pop();

                if (isOpenBracket(token))
                    stack.add(token);
                else{
                    if (! stack.isEmpty() && isCompleteBracket(stack.pop(), token));
                    else return false;
                }
            }
            return stack.isEmpty();
        }
    }

    private static boolean isCompleteBracket(String open, String close){
        return (open.equals("(") && close.equals(")")) || (open.equals("{") && close.equals("}")) || (open.equals("[") && close.equals("]"));
    }

    private static boolean isOpenBracket(String s){
        return s.equals("(") || s.equals("{") || s.equals("[");
    }
}
