import java.io.*;
import java.util.*;

public class Kruskal_MST_ReallySpecialSubtree {
    public static void main(String[] args) {
        FastScanner scanner = new FastScanner(System.in);
        int vertexCount = scanner.nextInt();
        int edgeCount = scanner.nextInt();


        WeightedGraph<Integer, Integer> graph = new WeightedGraph<>();
        for (int i = 0; i < edgeCount; i++){
            graph.addNode(scanner.nextInt(), scanner.nextInt(), scanner.nextInt());
        }
        System.out.println(solve(graph, new HashSet<>(), vertexCount+1));
    }

    private static Long solve(WeightedGraph<Integer, Integer> graph, Set<Integer> visited, int max){
        Long sum = 0L;
        Queue<WeightedEdge<Integer, Integer>> queue = new PriorityQueue<>();
        queue.addAll(graph.getAllEdges());

        Union union = new Union(max);
        while (! queue.isEmpty()){
            WeightedEdge<Integer, Integer> node = queue.poll();
            if (! union.connected(node.getNodeA(), node.getNodeB())){
                union.union(node.getNodeA(), node.getNodeB());
                sum += node.getWeight();
            }
        }
        return sum;
    }
    
    static class FastScanner {
    
        private BufferedReader reader;
        private StringTokenizer token;
    
        public FastScanner(InputStream in) {
            this.reader = new BufferedReader(new InputStreamReader(in));
        }
    
        private String next(){
            if (this.token == null || ! this.token.hasMoreTokens()){
                try {
                    this.token = new StringTokenizer(reader.readLine());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return this.token.nextToken();
        }
    
        public Integer nextInt(){
            return Integer.valueOf(next());
        }
    
        public Long nextLong(){
            return Long.valueOf(next());
        }
    
        public String nextString(){
            return next();
        }
    }
    
    static class Union {
        private int[] id;
        private int[] sz;
    
    
        public Union (int n) {
            id = new int[n];
            sz = new int[n];
            for (int i=0;i<n;i++) {
                id[i] = i;
                sz[i] = 1;
            }
        }
        int root(int p) {
            int pid = id[p];
            while (pid !=id[pid ])
                pid = id[pid ];
            return pid;
        }
        public int find(int p) { return root(p); }
    
        public void union(int p, int q) {
            int rp = root(p), rq = root(q);
            if (sz[rp] > sz[rq]) {
                id[rq] = rp; sz[rp] += sz[rq];
            }
            else {
                id[rp] = rq; sz[rq] += sz[rp];
            }
        }
    
        public boolean connected(int p, int q) {
            return find(p) == find(q);
        }
    }
    
    static class WeightedGraph<T extends Comparable<T>, Q extends Number & Comparable<Q>> {
        private Map<T, Set<WeightedEdge<T, Q>>> graph;
        private Set<WeightedEdge<T,Q>> uniqueEdges;
        private Integer edges;
        private WeightedEdge<T, Q> smallestNode;
    
        public WeightedGraph() {
            this.graph = new HashMap<>();
            this.edges = 0;
            this.uniqueEdges = new HashSet<>();
        }
    
        public void addNode(T u, T v, Q w){
            edges++;
            WeightedEdge<T, Q> node = new WeightedEdge<>(u, v, w);
            this.uniqueEdges.add(node);
    
            if (! this.graph.containsKey(u))
                this.graph.put(u, new HashSet<>());
    
    
            this.graph.get(u).add(node);
    
            if (smallestNode == null || w.compareTo(smallestNode.getWeight()) == -1)
                smallestNode = node;
    
        }
    
        public void union(T u, T v){
    
        }
    
        public Set<WeightedEdge<T,Q>> getAllEdges(){
            return uniqueEdges;
        }
    
        public boolean containVertex(T u){
            return graph.containsKey(u);
        }
    
        public Set<WeightedEdge<T, Q>> vertexOf(T u){
            return graph.get(u);
        }
    
        public Integer size(){
            return uniqueEdges.size();
        }
    
        public Integer edge(){
            return edges;
        }
    
        public WeightedEdge<T, Q> getSmallestNode() {
            return smallestNode;
        }
    
        @Override
        public String toString() {
            StringBuilder builder = new StringBuilder();
            builder.append("WeightedGraph {").append("\n");
            Iterator<T> itr = graph.keySet().iterator();
            while (itr.hasNext()) {
                T next = itr.next();
                builder.append("\t").append(next).append(" -> ").append(this.graph.get(next)).append("\n");
            }
    
            builder.append("}");
    
            return builder.toString();
        }
    }
    static class WeightedEdge<T extends Comparable<T>, Q extends Number & Comparable<Q>> implements Comparator<Q>, Comparable<WeightedEdge<T, Q>> {
        private T nodeA;
        private T nodeB;
        private Q weight;
    
        public WeightedEdge(T nodeA, T nodeB, Q weight) {
            this.nodeA = nodeA;
            this.nodeB = nodeB;
            this.weight = weight;
        }
    
        public T getNodeA() {
            return nodeA;
        }
    
        public T getNodeB() {
            return nodeB;
        }
    
        public Q getWeight() {
            return weight;
        }
    
        @Override
        public int compare(Q o1, Q o2) {
            return o1.compareTo(o2);
        }
    
        @Override
        public int compareTo(WeightedEdge<T, Q> o) {
            return this.weight.compareTo(o.weight);
        }
    
        @Override
        public String toString() {
            return "[" + this.nodeA +" -> " + this.nodeB+ ", weight=" + this.weight + "]";
        }
    }
}
